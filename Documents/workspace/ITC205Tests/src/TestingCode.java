
/**
 * TestingCode - For testing the git repository.
 * @author Peter Simpson
 * @author Matthew Parisi
 * @version 
 * 2 - 14th July, 2016
 */
public class TestingCode {
	
	public static void main(String 	args[]) {
		
		int i;
		
		//Prints a line to the console using value of i + 1
		for (i = 0; i < 10; i++) {
			System.out.println("This is line number " + (i+1));
		}
		//I added this in Bitbucket
		
	}

}
